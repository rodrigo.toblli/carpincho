const changeImages = () => {
  chrome.storage.local.get(['mood'], function (carpinchoArray) {
    console.log('Value currently is ' + JSON.stringify(carpinchoArray.mood));
    let images = document.getElementsByTagName("img");
    let sources = document.getElementsByTagName("source");
    let videos = document.getElementsByTagName("video");

    const toCarpincho = (sourceArray, carpinchoArray) => {
      for (let i = 0; i < sourceArray.length; i++) {
        let randomNum = Math.floor(Math.random() * carpinchoArray.length);
        let image = sourceArray[i];
        let tags = ['src', 'srcset', 'srcx2']
        for (let j = 0; j < tags.length; j++){
          image[tags[j]] = carpinchoArray[randomNum];
        }
        image.style.objectFit = 'cover';
        image.setAttribute("data-carpincho", true);
      }
    }
    toCarpincho(videos,carpinchoArray.mood.videos)
    toCarpincho(sources,carpinchoArray.mood.images)
    toCarpincho(images, carpinchoArray.mood.images);

    setInterval(() => {
      console.log('ask interval')
      let imageNotCarpinchized = (document.querySelectorAll('img:not([data-carpincho=true])'));
      let sourceNotCarpinchized = (document.querySelectorAll('source:not([data-carpincho=true])'));
      //spread not working
      toCarpincho(sourceNotCarpinchized, carpinchoArray.mood.images);
      toCarpincho(imageNotCarpinchized, carpinchoArray.mood.images);
    }, 2000)
  });
}

export const transformaAcarp = async (carpinchoMood) => {

  chrome.storage.local.set({ mood: carpinchoMood }, function () {
    console.log('Value is set to ' + carpinchoMood);
  });

  async function getCurrentTab() {
    let queryOptions = { active: true, currentWindow: true };
    let [tab] = await chrome.tabs.query(queryOptions);
    return tab;
  }
  const tabId = (await getCurrentTab()).id
  chrome.scripting.executeScript(
    {
      target: { tabId: tabId },
      func: changeImages,
    },
    () => { });
}
import './App.css';
import carpincho from './carpincho.svg';
import './Content'
import './utils/dbCarpincho'
import { transformaAcarp } from './Content';
import { dbCarpinchos } from './utils/dbCarpincho';

const CarpinchoManager = () => {
  return (
    <div className="App">
      <section className='title-carpincho-section'>
        <h2 className='title-carpincho'>Carpincho Tool</h2>
        <p className='subtitle-carpincho'>elija su estado de carpincho</p>
      </section>
     
     <section>
       <img className='logo' src={carpincho} alt='logo carpincho'/>
     </section>
     <section className='menu'>
        <p className='transformar' onClick={()=>{transformaAcarp(dbCarpinchos.chill)}}>chill</p>
        <p className='transformar' onClick={()=>{transformaAcarp(dbCarpinchos.angry)}}>notChill</p>
        <p className='transformar' onClick={()=>{transformaAcarp(dbCarpinchos.whatever)}}>whatever</p>
     </section>
    </div>
  );
}

export default CarpinchoManager;

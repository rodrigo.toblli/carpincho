import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import CarpinchoManager from './CarpinchoManager';

ReactDOM.render(
  <React.StrictMode>
    <CarpinchoManager />
  </React.StrictMode>,
  document.getElementById('root')
);

